///адреса хранения данных о поливе
#define avadr 0
#define bradr (avadr+sizeof(int))
#define lasadr (bradr+sizeof(int))
#define blabadr (lasadr+sizeof(int))
//#define timeadr (blabadr+sizeof(int))
//#define weekadr (timeadr+sizeof(int))
//#define pumpHadr (weekadr+sizeof(int))
//#define pumpMadr (pumpHadr+sizeof(int))

//метод для проверки параметров записанных в пзу
//если то что лежит в еепром отлшичается от дефолта то обновляем.
void checkEEPROM() {
  int avic = 255;
  int brah = 255;
  int lasid = 255;
  int blaber = 255;
 // int weeke = 255;
 // int pumpH = 255;
 // int pumpM = 255;
  /*адрес   что лежит
    0      av время полива авикулярий
    1      br время полива брах
    2      las время полива ласидоры
    3      blab время полива тараканов
  */
  EEPROM.begin(100);
  EEPROM.get(avadr, avic);
  EEPROM.get(bradr, brah);
  EEPROM.get(lasadr, lasid);
  EEPROM.get(blabadr, blaber);
//  EEPROM.get(timeadr, message);
  //EEPROM.get(weekadr, weeke);
 // EEPROM.get(pumpHadr, pumpH);
 // EEPROM.get(pumpMadr, pumpM);
  if (debug)Serial.println(avic);
  if (debug)Serial.println(brah);
  if (debug)Serial.println(lasid);
  if (debug)Serial.println(blaber);
  //if (debug)Serial.println(message);
 // if (debug)Serial.println(week);
 // if (debug)Serial.println(pumpH);
 // if (debug)Serial.println(pumpM);
  //кусок тупого кода, не хотелось с массивом заморачиваться
  if (avic != av & avic != 0 & avic != 255 & avic > 0 & avic < 50)av = avic;
  if (brah != br & brah != 0 & brah != 255 & brah > 0& brah < 50)br = brah;
  if (lasid != las & lasid != 0 & lasid != 255 & lasid > 0& lasid < 50)las = lasid;
  if (blaber != blab & blaber != 0 & blaber != 255 & blaber > 0& blaber < 50)blab = blaber;
  //if (weeke != week & weeke != 0 & weeke != 255 & weeke > 0& weeke < 8)week = weeke;
 // if (pumpH != hourpump & pumpH != 0 & pumpH != 255& pumpH < 24)hourpump = pumpH;
 // if (pumpM != minpump & pumpM != 0 & pumpM != 255& pumpM < 60)minpump = pumpM;
  EEPROM.end();
}


bool writeEEPROM(int val, byte adr) {
  int param = 0;
  if (debug) Serial.print("adr= ");
  if (debug) Serial.print(adr);
  if (debug) Serial.println(" ");
  EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.print("param= ");
  if (debug) Serial.print(param);
  if (debug) Serial.println(" ");
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    EEPROM.commit();
    return true;
  } else return false;
  EEPROM.end();
}

bool writeEEPROM_str(String val, byte adr) {
  String param ;
  EEPROM.begin(25);
  EEPROM.get(adr, param);
  if (debug) Serial.println(param);
  if (param != val) {
    EEPROM.put(adr, val);
    if (debug) Serial.print("write ");
    if (debug) {
      EEPROM.get(adr, param);
      Serial.println(param);
    }
    EEPROM.commit();
    return true;
  } else return false;
  EEPROM.end();
}
