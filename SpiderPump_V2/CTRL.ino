//метод для управления насосами и клапанами
void watering(byte valve, byte del) {
  if (debug) Serial.println("Pumping");
  digitalWrite(pump, HIGH);
  //delay(100);
  digitalWrite(valve, HIGH);
  delay(del * 1000);
  digitalWrite(pump, LOW);
  digitalWrite(valve, LOW);
  delay(100);
}


//метод для полива всех банок сразу, не выключаем насос
void pumping_all(byte del, byte del2, byte del3, byte del4) {
  /*
    byte valve_av = 4;
    byte valve_br = 14;
    byte valve_las = 12;
    byte valve_blab = 13;
  */
  if (debug)Serial.println("Pumping_ALL");
  digitalWrite(pump, HIGH);
  delay(100);
  digitalWrite(valve_av, HIGH);
  delay(del * 1000);
  digitalWrite(valve_av, LOW);
  digitalWrite(valve_br, HIGH);
  delay(del2 * 1000);
  digitalWrite(valve_br, LOW);
  digitalWrite(valve_las, HIGH);
  delay(del3 * 1000);
  digitalWrite(valve_las, LOW);
  digitalWrite(valve_blab, HIGH);
  delay(del4 * 1000);
  digitalWrite(valve_blab, LOW);
  digitalWrite(pump, LOW);
  delay(100);
}

/*
  //Полив всех банок
  void chkpump() {
  if (debug) Serial.println("chkpmp");
  //пришло время полива
  /* if (pumping) {
     if (debug) Serial.println(analogRead(A0));
     if (analogRead(A0) > 512 || debug) {
       if (debug) Serial.println("start pump all");
       pumping_all(av, br, las, blab);
       pumping = false;
     } else {
       //пишем сообщение
       Serial.println("Pump Failed,water level low");
       pumping = false;
     }
     level_publish();
     lastdata_publish(now(), true);
    }
  }
*/

void pumpReady() {
  if (weekday() == week) //пришел искомый день
    if (hour() == hourpump)//час как надо
      if (minute() >= minpump & minute() < minpump + 1) {
        if (debug) Serial.println("pump time!");
        pumping_all(av, br, las, blab);
        lastdata_publish(now(), true);
        delay(120000);
      }
}
