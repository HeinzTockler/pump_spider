///http://arduino.esp8266.com/stable/package_esp8266com_index.json
#include <ESP8266WiFi.h>
#include <TimeLib.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <PubSubClient.h>
#include <EEPROM.h>

/////////////Вафля////////////////////////////////////////////////////////////
const char* ssid = ""; //  your network SSID (name)
const char* password = ""; // your network password

//////////Настройки MQTT//////////////////////////////////////////////////////
const char *mqtt_server = "192.168.11.4"; // Имя сервера MQTT
const int mqtt_port = 1883; // Порт для подключения к серверу MQTT
const char *mqtt_user = "heinz"; // Логи для подключения к серверу MQTT
const char *mqtt_pass = "192422901"; // Пароль для подключения к серверу MQTT
WiFiClient wclient;
PubSubClient client(wclient);

//////////NTP//////////////////////////////////////////////////////
//static const char ntpServerName[] = "192.168.1.2";
IPAddress timeServer(192, 168, 1, 2);
const int timeZone = 8;
const int NTP_PACKET_SIZE = 48; // NTP time is in the first 48 bytes of message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming & outgoing packets
WiFiUDP Udp;
unsigned int localPort = 2390;      // local port to listen for UDP packets

///////Переменные/////////////////////////////////////////////////////////////
unsigned long curTime = 0; //для организации циклов прерывания3
//unsigned long hourTime = 0; //для организации циклов прерывания
unsigned long publicTime = 0; //для организации циклов прерывания
unsigned long cicle_update = 5000; //основной цикл минуты
unsigned long cicle_public = 60000; // цикл для публикации данных
unsigned long lastReconnectAttempt = 0;
bool pumping = false; //активирован режим полива
time_t lastpumpTime; //время последнего полива
bool debug = true;
char message[30]; //сюда кэшируем дату

//////Входа///////////////////////////////////////////////////////////////////
int pump = 5;
//макс изм напряжение ADC это 1в, а не 3.3.
int valve_av = 4;
int valve_br = 14;
int valve_las = 12;
int valve_blab = 13;
//Время работы насоса в секундах
int av = 15;
int br = 22;
int las = 10;
int blab = 2;
//Для работы автополива по времени
int week = 0;
int hourpump = 12;
int minpump = 2;

//void watering(byte valve, byte del);
//bool writeEEPROM(int val, byte adr);

void setup() {
  Serial.begin(115200);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("Connection Failed! Rebooting...");
    delay(5000);
    ESP.restart();
  }

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname("SpiderPump");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_SPIFFS
      type = "filesystem";
    }

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();

  pinMode(pump, OUTPUT);
  pinMode(valve_av, OUTPUT);
  pinMode(valve_br, OUTPUT);
  pinMode(valve_las, OUTPUT);
  pinMode(valve_blab, OUTPUT);
 // pinMode(A0, INPUT);

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);

  wifi_update();
  mqqt_check();
  curTime = millis();
  publicTime = millis();
  // testTime = millis();
  setSyncProvider(getNtpTime);
  setSyncInterval(46900);//интервал синхронизации
  checkEEPROM();//смотрим че там в ПЗУ, если что обновляем

  lastdata_publish(now(), false);
 // level_publish();
  curdata_publish();
  pumptime_publish();
  daypump_publish();

  if (debug) Serial.println("Setup_end");
}

void loop() {
  //3600000 = час
  //86400000 = сутки
  ArduinoOTA.handle();
  mqqt_check();
  
  //основной цикл 3сек
  if (millis() > (curTime + cicle_update)) {
    curTime = millis();
    //if (debug) Serial.println("======================================================================");
    pumpReady();
  } else if (curTime > millis()) {
    curTime = millis();
    Serial.println("Overflov cicle_update");
  }

  //часовой цикл
  if (millis() > (publicTime +cicle_public)) {
    publicTime = millis();
    
    wifi_update();
    lastdata_publish(lastpumpTime, false);
    curdata_publish();
    pumptime_publish();
    daypump_publish();

  } else if (publicTime > millis()) {
    publicTime = millis();
    Serial.println("Overflov cicle_hour");
  }
}
