void callback(char* topic, byte* payload, unsigned int length) {
  String str;
  if (debug)Serial.print("Message arrived [");
  if (debug)Serial.print(topic);
  if (debug)Serial.print("] ");
  if (debug) Serial.print(length);
  if (debug) Serial.print("payload=");

  for (int i = 0; i < length; i++) {
    if (debug) Serial.print((char)payload[i]);
    str += (char)payload[i];
  }
  if (debug) Serial.println();

  if ((String)topic == "pump/publish") {
    lastdata_publish(lastpumpTime, false);
    // level_publish();
    curdata_publish();
    pumptime_publish();
    daypump_publish();
  }

  if ((String)topic == "pump/manual") {
    if (str == "all")pumping_all(av, br, las, blab);
    if (str == "av")watering(valve_av, av);
    if (str == "br")watering(valve_br, br);
    if (str == "las") watering(valve_las, las);
    if (str == "blab")watering(valve_blab, blab);
  }
  /*адрес   что лежит
    0      av время полива авикулярий
    1      br время полива брах
    2      las время полива ласидоры
    3      blab время полива тараканов
  */
  if ((String)topic == "pump/timeav") {
    if (debug)Serial.println(str.toInt());
    if (writeEEPROM(str.toInt(), avadr))av = str.toInt();
  }
  if ((String)topic == "pump/timebr") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), bradr))br = str.toInt();
  }
  if ((String)topic == "pump/timelas") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), lasadr))las = str.toInt();
  }
  if ((String)topic == "pump/timeblab") {
    if (debug)Serial.println(str.toInt());
    if ( writeEEPROM(str.toInt(), blabadr))blab = str.toInt();
  }
  /*
    if ((String)topic == "pump/setTime") {
    //12.02/5
    if (debug) Serial.println(str);
    if (debug) Serial.println(str.length());
    if (str.length() > 0 & str.length() < 8) {
      String str1;
      String str2;
      String str3;
      str1 = str.substring(0, 2);
      if (debug)Serial.println(str1);
      if (debug)Serial.println(str1.toInt());
      str2 = str.substring(3, 5);
      if (debug)Serial.println(str2);
      if (debug)Serial.println(str2.toInt());
      str3 = str.substring(5, 6);
      if (debug)Serial.println(str3);
      if (debug)Serial.println(str3.toInt());
      if (str1.toInt() < 24 & str1.toInt()>0)
        if (str2.toInt()< 60 & str2.toInt()>0)
          if (str3.toInt()< 8 & str3.toInt()>0) {
            if (debug)Serial.println("command right");
            hourpump = str1.toInt();
            writeEEPROM(hourpump, pumpHadr);
            minpump = str2.toInt();
            writeEEPROM(minpump, pumpMadr);
            week = str3.toInt();
            writeEEPROM(week, weekadr);
          }
    }
    }
  */
  delay(100);
}
// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress & address)
{
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12] = 49;
  packetBuffer[13] = 0x4E;
  packetBuffer[14] = 49;
  packetBuffer[15] = 52;
  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

time_t getNtpTime()
{
  IPAddress ntpServerIP; // NTP server's ip address
  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  // get a random server from the pool
  Udp.begin(localPort);
  //WiFi.hostByName(ntpServerName, ntpServerIP);
  //Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(timeServer);
  sendNTPpacket(timeServer);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
      Udp.flush();
      Udp.stop();
    }
  }
  Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}

boolean reconnect() {
  //if (debug) Serial.println("Reconnect");
  if (client.connect("SpiderPump", mqtt_user, mqtt_pass)) {
    // if (debug) Serial.println("MQQT Connection UP");
    //client.publish("pump/LastTime", "");
    //client.publish("pump/Level", "");
    // client.publish("pump/curTime", "");

    client.subscribe("pump/manual");
    client.subscribe("pump/timeav");
    client.subscribe("pump/timebr");
    client.subscribe("pump/timelas");
    client.subscribe("pump/timeblab");
   // client.subscribe("pump/setTime");
  }
  return client.connected();
}

void mqqt_check() {
  //if (debug) Serial.print("MQQT");
  if (!client.connected()) {
    if (millis() - lastReconnectAttempt > 5000) {
      // if (debug) Serial.println(millis());
      lastReconnectAttempt = millis();
      if (reconnect()) {
        lastReconnectAttempt = 0;
      }
    }
    // if (debug) Serial.println(" reconnect");
  } else {
    client.loop();
    // if (debug) Serial.println(" loop");
  }
}

/*
  void level_publish() {
  if (debug) Serial.println("Publish_level");
  char message[10];
  String str;
  if (debug) Serial.println(analogRead(A0));
  if (analogRead(A0) > 600) {
    str = "high";
  } else {
    str = "low";
  }
  str.toCharArray(message, 10);
  if (debug) Serial.println(message);
  client.publish("pump/Level", message);
  }*/

void pumptime_publish() {
  if (debug) Serial.println("Publish_pump time");
  char message[15];
  String str;
  str += 'a';
  str += av;
  str += 'b';
  str += br;
  str += 'l';
  str += las;
  str += 'b';
  str += blab;
  str.toCharArray(message, 15);
  if (debug) Serial.println(message);
  client.publish("pump/pumptime", message);
}

void daypump_publish() {
  if (debug) Serial.println("Publish_daypump");
  char message[15];
  String str;
  //int week = 7;
  // int hourpump = 12;
  //int minpump = 2;
  str += 'w';
  str += week;
  str += 'h';
  str += hourpump;
  str += 'm';
  str += minpump;
  str.toCharArray(message, 15);
  if (debug) Serial.println(message);
  client.publish("pump/daypump", message);
}

//lastpumpTime
void lastdata_publish(time_t time_pump, bool eepwrite) {
  if (debug) Serial.print("Publish_date  ");
  if (debug) Serial.println(message);
  if (eepwrite) {
    timeToString(time_pump).toCharArray(message, 20);
    client.publish("pump/LastTime", message);
    //writeEEPROM_str(message, timeadr);
    lastpumpTime = time_pump;
  } else {
    timeToString(lastpumpTime).toCharArray(message, 20);
    client.publish("pump/LastTime", message);
  }
}

//lastpumpTime
void curdata_publish() {
  timeToString(now()).toCharArray(message, 20);
  client.publish("pump/curTime", message);
}

String timeToString(time_t t) {
  String str;
  str += day(t);
  str += '.';
  str += month(t);
  str += '.';
  str += year(t);
  str += '/';
  str += hour(t);
  str += ':';
  str += minute(t);
  str += '/';
  str += weekday(t);
  return str;
}
