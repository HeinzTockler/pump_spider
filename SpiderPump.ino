#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <TimeLib.h>
#include <PubSubClient.h>

/////////////Вафля///////////////////////////////////////////////////////////////
const char* ssid = "Redmi";  //  your network SSID (name)
const char* password = "192422901";       // your network password

/////////////Синхронизация времени/////////////////////////////////////////////////
unsigned int localPort = 2390;      // local port to listen for UDP packets
IPAddress timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName = "1.europe.pool.ntp.org";
const int timeZone = 8;
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
WiFiUDP Udp; // A UDP instance to let us send and receive packets over UDP

//////////Настройки MQTT//////////////////////////////////////////////////////////
const char *mqtt_server = "m20.cloudmqtt.com"; // Имя сервера MQTT
const int mqtt_port = 13806; // Порт для подключения к серверу MQTT
const char *mqtt_user = "hutrppac"; // Логи для подключения к серверу MQTT
const char *mqtt_pass = "wTHG0GaYh_7E"; // Пароль для подключения к серверу MQTT
WiFiClient wclient;
PubSubClient client(wclient);

/////////Сигнатуры///////////////////////////////////////////////////////////
time_t getNtpTime();
void sendNTPpacket(IPAddress &address);
void Wifi_setup();
void digitalClockDisplay();
void printDigits(int digits);
void timeSync();
void chkpump();
void watering(byte valve, byte del);
void callback(char* topic, byte* payload, unsigned int length);
void reconnect();
bool wifi_status();
bool wifi_reconnect(byte trycount);
void level_publish();
void lastdata_publish(time_t time_pump);

///////Переменные/////////////////////////////////////////////////////////////
//time_t prevDisplay = 0;
unsigned long curTime = 0; //для организации циклов прерывания
//unsigned long prevTime = 0;
//const int intervalHour = 3600000;//1 час
const int interval = 3000; //основной цикл
const byte Sync_interval = 3;//интервал синхронизации времени
byte SyncDay = 255;
//byte curDay = 255;
byte prevDay = 255;//день когда была совершена синхронизация времени
bool pumping = false; //активирован режим полива
time_t lastpumpTime; //время последнего полива
bool debug = true;
//Ниже переменные для ручного запуска отдельных банок
bool av_manual = false;
bool br_manual = false;
bool las_manual = false;
bool blab_manual = false;

//////Входа//////////////////////////////////////////////////////////////////////
byte pump = 5;
byte level = 0;
byte valve_av = 4;
byte valve_br = 16;
byte valve_las = 12;
byte valve_blab = 13;

void setup()
{
  Serial.begin(115200);

  pinMode(pump, OUTPUT);
  pinMode(valve_av, OUTPUT);
  pinMode(valve_br, OUTPUT);
  pinMode(valve_las, OUTPUT);
  pinMode(valve_blab, OUTPUT);
  pinMode(level, INPUT);
  //curDay = day();
  // ESP.MODEM-SLEEP(20e6);
 // Wifi_setup();

  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  prevDay = day();
  curTime = millis();
}

void loop()
{
  if (millis() > (curTime + interval)) {
    curTime = millis();
    digitalClockDisplay();
    //if (debug)pumping = true;
    //суббота, полив не активирован, и 10 часов
    if (weekday() == 0 & pumping == false & hour() == 10 & day(lastpumpTime) != day()) pumping = true;
    chkpump();
    /*Serial.print("level= ");
      Serial.print(analogRead(level));
      Serial.println("");*/
    if (!client.connected()) {
      reconnect();
    } else
      client.loop();
  } else if (curTime > millis()) {
    curTime = millis();
    Serial.println("Overflov");
  }

  //раз в день проверяем не настал ли новый день и не пора ли синхронизироваться
  if (day() != prevDay)
    if (wifi_status()) {
      timeSync();
      prevDay = day();
      Serial.print("prevDay= ");
      Serial.println(prevDay);
    } else {
      wifi_reconnect(3);
      prevDay = day();
      Serial.print("prevDay= ");
      Serial.println(prevDay);
    }

  if (SyncDay == 255)timeSync();
}

bool wifi_reconnect(byte trycount) {
  if (debug)Serial.print("TryConnect");
  byte i = 0;
  while (i < trycount && !wifi_status()) {
    delay(1000);
    Serial.print(".");
    i++;
  }
  if (wifi_status()) return true;
  else return false;
}

bool wifi_status() {
  if (WiFi.status() == WL_CONNECTED) return true;
  else return false;
}

//синхронизируем время через daySync дней
void timeSync() {
  Serial.print("weekday= ");
  Serial.println(weekday());
  if ((SyncDay == 255) || (day() - SyncDay) >= Sync_interval) {
    if (wifi_status()) {
      setSyncProvider(getNtpTime);
      setSyncInterval(300);
      SyncDay = day();
    }
  }
}

void chkpump() {
  //пришло время полива
  if (pumping) {
    lastpumpTime = now();
    if (analogRead(level) > 512 || debug) {
      Serial.println("start pump");
      watering(valve_av, 30);
      watering(valve_br, 20);
      watering(valve_las, 20);
      watering(valve_blab, 30);
      pumping = false;
    } else {
      //пишем сообщение
      Serial.println("Pump Failed,water level low");
      pumping = false;
    }
    level_publish();
    lastdata_publish(lastpumpTime);
  }
}

//метод для управления насосами и клапанами
void watering(byte valve, byte del) {
  Serial.println("Pumping");
  digitalWrite(pump, HIGH);
  delay(100);
  digitalWrite(valve, HIGH);
  delay(del * 100);
  digitalWrite(pump, LOW);
  digitalWrite(valve, LOW);
  delay(100);
}

void digitalClockDisplay()
{
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  Serial.print(" ");
  Serial.print(day());
  Serial.print(".");
  Serial.print(month());
  Serial.print(".");
  Serial.print(year());
  Serial.print(" weekday= ");
  Serial.print(weekday());
  Serial.println();
}

void printDigits(int digits)
{
  // utility for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if (digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

// We start by connecting to a WiFi network
void Wifi_setup() {
  byte i = 0;
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  //пробуем подконектится 10 раз с интервалом в 3 сек.
  while (i < 10 && !wifi_status()) {
    delay(1000);
    Serial.print("_");
    Serial.print(i);
    delay(3000);
    i++;
  }
  Serial.println();

  if (wifi_status()) {
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
    Serial.println("waiting for sync");
    //setSyncProvider(getNtpTime);
   // setSyncInterval(300);
  } else Serial.println("Sync stopped, wifi is not found");
}

// send an NTP request to the time server at the given address
void sendNTPpacket(IPAddress & address)
{
  Serial.println("sending NTP packet...");
  memset(packetBuffer, 0, NTP_PACKET_SIZE);
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;
  Udp.beginPacket(address, 123); //NTP requests are to port 123
  Udp.write(packetBuffer, NTP_PACKET_SIZE);
  Udp.endPacket();
}

time_t getNtpTime()
{
  IPAddress ntpServerIP; // NTP server's ip address

  while (Udp.parsePacket() > 0) ; // discard any previously received packets
  Serial.println("Transmit NTP Request");
  // get a random server from the pool
  Udp.begin(localPort);
  WiFi.hostByName(ntpServerName, ntpServerIP);
  Serial.print(ntpServerName);
  Serial.print(": ");
  Serial.println(ntpServerIP);
  sendNTPpacket(ntpServerIP);
  uint32_t beginWait = millis();
  while (millis() - beginWait < 1500) {
    int size = Udp.parsePacket();
    if (size >= NTP_PACKET_SIZE) {
      Serial.println("Receive NTP Response");
      Udp.read(packetBuffer, NTP_PACKET_SIZE);  // read packet into the buffer
      unsigned long secsSince1900;
      // convert four bytes starting at location 40 to a long integer
      secsSince1900 =  (unsigned long)packetBuffer[40] << 24;
      secsSince1900 |= (unsigned long)packetBuffer[41] << 16;
      secsSince1900 |= (unsigned long)packetBuffer[42] << 8;
      secsSince1900 |= (unsigned long)packetBuffer[43];
      return secsSince1900 - 2208988800UL + timeZone * SECS_PER_HOUR;
      Udp.flush();
      Udp.stop();
    }
  }
  Serial.println("No NTP Response :-(");
  return 0; // return 0 if unable to get the time
}

void reconnect() {
  // Loop until we're reconnected
  //  while (!client.connected())
  if (wifi_status()) {
    if (!client.connected()) {
      Serial.print("Attempting MQTT connection...");
      // Create a random client ID
      String clientId = "ESP8266Pump";
      //clientId += String(random(0xffff), HEX);
      // Attempt to connect
      if (client.connect(clientId.c_str(), mqtt_user, mqtt_pass)) {
        Serial.println("connected");
        // Once connected, publish an announcement...
        client.publish("pump/LastTime", "дата");
        client.publish("pump/Level", "level");
        // ... and resubscribe
        client.subscribe("pump/manualRun");
      } else {
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
        // Wait 5 seconds before retrying
        //delay(5000);
      }
    }
  } else wifi_reconnect(3);
  // Serial.println();
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if (topic == "manualRun")pumping = true;
  // Serial.println("приехало"); // выводим в сериал порт подтверждение, что мы получили топик test/2
}

void level_publish() {
  bool lev = false;
  if (analogRead(level) > 512) lev = true; else lev = false;
  //    client.publish("pump/Level", (String)lev );
}

void lastdata_publish(time_t time_pump) {
  /* String buf;
    buf += time_pump.day();
    buf += "/";
    buf += time_pump.month();
    buf += "/";
    buf += time_pump.year();
    buf += "_";
    buf += time_pump.hour();
    buf += ":";
    buf += time_pump.minute();
    client.publish("pump/LastTime", buf );*/
}

